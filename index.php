<?php 

$file = file_get_contents('./input.txt', true);

$input = explode(PHP_EOL, $file);

$valid_count = 0;

foreach($input as $a) {
	preg_match_all('!\d+!', $a, $matches);

	$char = substr($a, strpos($a, ":")-1, 1);
	$min = $matches[0][0];
	$max = $matches[0][1];

	$first_check = 0;
	$second_check = 0;

	$string = substr($a, strpos($a, ":")+1);

	$count = 0;

	if($string[$min] == $char) {
		$first_check = 1;
	}
	if($string[$max] == $char) {
		$second_check = 1;
	}

	if($first_check == true && $second_check == false) {
		$valid_count++;
	}
	else if($first_check == false && $second_check == true) {
		$valid_count++;
	}

}

print_r($valid_count);
die();
 
?>